#!/bin/bash

/usr/lib/qt6/uic -g python ./ui-src/timetable.ui -o ./assets/ui/timetable_ui.py
/usr/lib/qt6/uic -g python ./ui-src/subject.ui -o ./assets/ui/subject_ui.py
/usr/lib/qt6/uic -g python ./ui-src/settings.ui -o ./assets/ui/settings_ui.py
/usr/lib/qt6/uic -g python ./ui-src/themes.ui -o ./assets/ui/themes_ui.py
/usr/lib/qt6/uic -g python ./ui-src/buy.ui -o ./assets/ui/buy_ui.py
/usr/lib/qt6/uic -g python ./ui-src/installer.ui -o ./assets/ui/installer_ui.py
/usr/lib/qt6/uic -g python ./ui-src/custom.ui -o ./assets/ui/custom_ui.py
/usr/lib/qt6/uic -g python ./ui-src/assignment.ui -o ./assets/ui/assignment_ui.py
/usr/lib/qt6/uic -g python ./ui-src/assignmentswindow.ui -o ./assets/ui/assignmentswindow_ui.py
/usr/lib/qt6/uic -g python ./ui-src/assignmentsadd.ui -o ./assets/ui/assignmentsadd_ui.py
/usr/lib/qt6/uic -g python ./ui-src/map.ui -o ./assets/ui/map_ui.py
/usr/lib/qt6/rcc -g python ./assets/backgrounds/backgrounds.qrc -o ./assets/ui/backgrounds_rc.py
/usr/lib/qt6/rcc -g python ./assets/screenshots/screenshots.qrc -o ./assets/ui/screenshots_rc.py
