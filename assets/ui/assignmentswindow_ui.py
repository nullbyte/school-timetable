# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'assignmentswindow.ui'
##
## Created by: Qt User Interface Compiler version 6.2.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QDialog, QFrame, QHBoxLayout,
    QLabel, QPushButton, QScrollArea, QSizePolicy,
    QSpacerItem, QVBoxLayout, QWidget)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.setWindowModality(Qt.ApplicationModal)
        Dialog.resize(420, 420)
        self.verticalLayout_2 = QVBoxLayout(Dialog)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(6, -1, 6, -1)
        self.frame = QFrame(Dialog)
        self.frame.setObjectName(u"frame")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.verticalLayout = QVBoxLayout(self.frame)
        self.verticalLayout.setSpacing(3)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.header = QHBoxLayout()
        self.header.setObjectName(u"header")
        self.header.setContentsMargins(-1, 3, 5, 3)
        self.labelTitle = QLabel(self.frame)
        self.labelTitle.setObjectName(u"labelTitle")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelTitle.sizePolicy().hasHeightForWidth())
        self.labelTitle.setSizePolicy(sizePolicy)
        self.labelTitle.setMargin(3)
        self.labelTitle.setIndent(5)

        self.header.addWidget(self.labelTitle)

        self.buttonClose = QPushButton(self.frame)
        self.buttonClose.setObjectName(u"buttonClose")
        sizePolicy1 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.buttonClose.sizePolicy().hasHeightForWidth())
        self.buttonClose.setSizePolicy(sizePolicy1)
        icon = QIcon()
        iconThemeName = u"view-close"
        if QIcon.hasThemeIcon(iconThemeName):
            icon = QIcon.fromTheme(iconThemeName)
        else:
            icon.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)
        
        self.buttonClose.setIcon(icon)

        self.header.addWidget(self.buttonClose)


        self.verticalLayout.addLayout(self.header)

        self.assignmentScrollArea = QScrollArea(self.frame)
        self.assignmentScrollArea.setObjectName(u"assignmentScrollArea")
        self.assignmentScrollArea.setFrameShape(QFrame.NoFrame)
        self.assignmentScrollArea.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        self.assignmentScrollArea.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.assignmentScrollArea.setWidgetResizable(True)
        self.scrollAreaWidgetContents = QWidget()
        self.scrollAreaWidgetContents.setObjectName(u"scrollAreaWidgetContents")
        self.scrollAreaWidgetContents.setGeometry(QRect(0, 0, 398, 304))
        self.verticalLayout_3 = QVBoxLayout(self.scrollAreaWidgetContents)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.assignmentArea = QFrame(self.scrollAreaWidgetContents)
        self.assignmentArea.setObjectName(u"assignmentArea")
        self.assignmentArea.setFrameShape(QFrame.NoFrame)
        self.assignmentArea.setFrameShadow(QFrame.Raised)
        self.verticalLayout_4 = QVBoxLayout(self.assignmentArea)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.verticalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.assignmentAreaVerticalLayout = QVBoxLayout()
        self.assignmentAreaVerticalLayout.setObjectName(u"assignmentAreaVerticalLayout")
        self.labelNone = QLabel(self.assignmentArea)
        self.labelNone.setObjectName(u"labelNone")
        sizePolicy2 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.labelNone.sizePolicy().hasHeightForWidth())
        self.labelNone.setSizePolicy(sizePolicy2)
        self.labelNone.setAlignment(Qt.AlignCenter)

        self.assignmentAreaVerticalLayout.addWidget(self.labelNone)


        self.verticalLayout_4.addLayout(self.assignmentAreaVerticalLayout)

        self.spacer = QLabel(self.assignmentArea)
        self.spacer.setObjectName(u"spacer")

        self.verticalLayout_4.addWidget(self.spacer)


        self.verticalLayout_3.addWidget(self.assignmentArea)

        self.assignmentScrollArea.setWidget(self.scrollAreaWidgetContents)

        self.verticalLayout.addWidget(self.assignmentScrollArea)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(-1, 3, 5, 3)
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.buttonAdd = QPushButton(self.frame)
        self.buttonAdd.setObjectName(u"buttonAdd")
        icon1 = QIcon()
        iconThemeName = u"add"
        if QIcon.hasThemeIcon(iconThemeName):
            icon1 = QIcon.fromTheme(iconThemeName)
        else:
            icon1.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)
        
        self.buttonAdd.setIcon(icon1)

        self.horizontalLayout.addWidget(self.buttonAdd)


        self.verticalLayout.addLayout(self.horizontalLayout)


        self.verticalLayout_2.addWidget(self.frame)


        self.retranslateUi(Dialog)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Dialog", None))
        self.labelTitle.setText(QCoreApplication.translate("Dialog", u"<html><head/><body><p><span style=\" font-size:14pt; font-weight:600;\">Assignments</span></p></body></html>", None))
        self.buttonClose.setText("")
        self.labelNone.setText(QCoreApplication.translate("Dialog", u"No assignments, lucky you!", None))
        self.spacer.setText("")
        self.buttonAdd.setText("")
    # retranslateUi

