# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'installer.ui'
##
## Created by: Qt User Interface Compiler version 6.2.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QCheckBox, QGroupBox, QHBoxLayout,
    QLabel, QLineEdit, QPushButton, QRadioButton,
    QSizePolicy, QSpacerItem, QVBoxLayout, QWidget,
    QWizard, QWizardPage)

class Ui_Wizard(object):
    def setupUi(self, Wizard):
        if not Wizard.objectName():
            Wizard.setObjectName(u"Wizard")
        Wizard.resize(350, 300)
        Wizard.setMinimumSize(QSize(300, 280))
        icon = QIcon(QIcon.fromTheme(u"run-install"))
        Wizard.setWindowIcon(icon)
        Wizard.setSizeGripEnabled(False)
        Wizard.setWizardStyle(QWizard.ModernStyle)
        Wizard.setOptions(QWizard.NoBackButtonOnLastPage|QWizard.NoBackButtonOnStartPage|QWizard.NoCancelButtonOnLastPage)
        self.wizardPage1 = QWizardPage()
        self.wizardPage1.setObjectName(u"wizardPage1")
        self.verticalLayout = QVBoxLayout(self.wizardPage1)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.labelTitle = QLabel(self.wizardPage1)
        self.labelTitle.setObjectName(u"labelTitle")
        self.labelTitle.setStyleSheet(u"font: 75 16pt;")
        self.labelTitle.setMargin(5)
        self.labelTitle.setIndent(7)

        self.verticalLayout.addWidget(self.labelTitle)

        self.groupBoxOptions = QGroupBox(self.wizardPage1)
        self.groupBoxOptions.setObjectName(u"groupBoxOptions")
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.groupBoxOptions.sizePolicy().hasHeightForWidth())
        self.groupBoxOptions.setSizePolicy(sizePolicy)
        self.groupBoxOptions.setAlignment(Qt.AlignHCenter|Qt.AlignTop)
        self.groupBoxOptions.setFlat(False)
        self.groupBoxOptions.setCheckable(False)
        self.verticalLayout_2 = QVBoxLayout(self.groupBoxOptions)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(-1, 0, -1, -1)
        self.radioInstall = QRadioButton(self.groupBoxOptions)
        self.radioInstall.setObjectName(u"radioInstall")
        self.radioInstall.setChecked(True)

        self.verticalLayout_2.addWidget(self.radioInstall)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(15, -1, -1, -1)
        self.labelInstall = QLabel(self.groupBoxOptions)
        self.labelInstall.setObjectName(u"labelInstall")
        self.labelInstall.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout.addWidget(self.labelInstall)

        self.editPath = QLineEdit(self.groupBoxOptions)
        self.editPath.setObjectName(u"editPath")

        self.horizontalLayout.addWidget(self.editPath)

        self.buttonEditPath = QPushButton(self.groupBoxOptions)
        self.buttonEditPath.setObjectName(u"buttonEditPath")
        icon1 = QIcon()
        iconThemeName = u"fileopen"
        if QIcon.hasThemeIcon(iconThemeName):
            icon1 = QIcon.fromTheme(iconThemeName)
        else:
            icon1.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)
        
        self.buttonEditPath.setIcon(icon1)

        self.horizontalLayout.addWidget(self.buttonEditPath)


        self.verticalLayout_2.addLayout(self.horizontalLayout)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(110, -1, -1, -1)
        self.checkBoxDesktop = QCheckBox(self.groupBoxOptions)
        self.checkBoxDesktop.setObjectName(u"checkBoxDesktop")

        self.horizontalLayout_2.addWidget(self.checkBoxDesktop)


        self.verticalLayout_2.addLayout(self.horizontalLayout_2)

        self.radioUninstall = QRadioButton(self.groupBoxOptions)
        self.radioUninstall.setObjectName(u"radioUninstall")

        self.verticalLayout_2.addWidget(self.radioUninstall)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalLayout_3.setContentsMargins(15, -1, -1, -1)
        self.checkBoxData = QCheckBox(self.groupBoxOptions)
        self.checkBoxData.setObjectName(u"checkBoxData")

        self.horizontalLayout_3.addWidget(self.checkBoxData)


        self.verticalLayout_2.addLayout(self.horizontalLayout_3)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_2.addItem(self.verticalSpacer)


        self.verticalLayout.addWidget(self.groupBoxOptions)

        Wizard.addPage(self.wizardPage1)
        self.wizardPage2 = QWizardPage()
        self.wizardPage2.setObjectName(u"wizardPage2")
        self.labelTitle2 = QLabel(self.wizardPage2)
        self.labelTitle2.setObjectName(u"labelTitle2")
        self.labelTitle2.setGeometry(QRect(20, 20, 301, 31))
        self.labelTitle2.setStyleSheet(u"font: 75 16pt;")
        self.labelDetails = QLabel(self.wizardPage2)
        self.labelDetails.setObjectName(u"labelDetails")
        self.labelDetails.setGeometry(QRect(50, 100, 271, 41))
        self.labelDetails.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignTop)
        self.labelDetails.setWordWrap(True)
        Wizard.addPage(self.wizardPage2)

        self.retranslateUi(Wizard)

        QMetaObject.connectSlotsByName(Wizard)
    # setupUi

    def retranslateUi(self, Wizard):
        Wizard.setWindowTitle(QCoreApplication.translate("Wizard", u"Installer", None))
        self.labelTitle.setText(QCoreApplication.translate("Wizard", u"<strong>Manage Application", None))
        self.groupBoxOptions.setTitle(QCoreApplication.translate("Wizard", u"Options", None))
        self.radioInstall.setText(QCoreApplication.translate("Wizard", u"Install", None))
        self.labelInstall.setText(QCoreApplication.translate("Wizard", u"Install location:", None))
        self.editPath.setText(QCoreApplication.translate("Wizard", u"~/.local/share/app-name", None))
        self.editPath.setPlaceholderText(QCoreApplication.translate("Wizard", u"Path to install files", None))
        self.buttonEditPath.setText("")
        self.checkBoxDesktop.setText(QCoreApplication.translate("Wizard", u"Create desktop shortcut", None))
        self.radioUninstall.setText(QCoreApplication.translate("Wizard", u"Uninstall", None))
        self.checkBoxData.setText(QCoreApplication.translate("Wizard", u"Delete application data", None))
        self.labelTitle2.setText(QCoreApplication.translate("Wizard", u"<html><head/><body><p><span style=\" font-weight:600;\">Installing, please wait....</span></p></body></html>", None))
        self.labelDetails.setText(QCoreApplication.translate("Wizard", u"<html><head/><body><p>The application is being installed, please wait.</p></body></html>", None))
    # retranslateUi

