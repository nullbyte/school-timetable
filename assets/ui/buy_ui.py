# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'buy.ui'
##
## Created by: Qt User Interface Compiler version 6.2.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QDialog, QFrame, QHBoxLayout,
    QLabel, QPushButton, QSizePolicy, QSpacerItem,
    QVBoxLayout, QWidget)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(416, 298)
        self.horizontalLayout = QHBoxLayout(Dialog)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.frameBuy = QFrame(Dialog)
        self.frameBuy.setObjectName(u"frameBuy")
        self.frameBuy.setFrameShape(QFrame.NoFrame)
        self.frameBuy.setFrameShadow(QFrame.Raised)
        self.verticalLayout = QVBoxLayout(self.frameBuy)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.pitchLabel = QLabel(self.frameBuy)
        self.pitchLabel.setObjectName(u"pitchLabel")
        self.pitchLabel.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignTop)
        self.pitchLabel.setWordWrap(True)

        self.verticalLayout.addWidget(self.pitchLabel)

        self.verticalSpacer = QSpacerItem(20, 105, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.buttonBack = QPushButton(self.frameBuy)
        self.buttonBack.setObjectName(u"buttonBack")

        self.horizontalLayout_3.addWidget(self.buttonBack)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer)

        self.buttonInfo = QPushButton(self.frameBuy)
        self.buttonInfo.setObjectName(u"buttonInfo")

        self.horizontalLayout_3.addWidget(self.buttonInfo)

        self.buttonLoad = QPushButton(self.frameBuy)
        self.buttonLoad.setObjectName(u"buttonLoad")

        self.horizontalLayout_3.addWidget(self.buttonLoad)


        self.verticalLayout.addLayout(self.horizontalLayout_3)


        self.horizontalLayout.addWidget(self.frameBuy)

        self.frameInfo = QFrame(Dialog)
        self.frameInfo.setObjectName(u"frameInfo")
        self.frameInfo.setFrameShape(QFrame.NoFrame)
        self.frameInfo.setFrameShadow(QFrame.Raised)
        self.verticalLayout_2 = QVBoxLayout(self.frameInfo)
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.infoLabel = QLabel(self.frameInfo)
        self.infoLabel.setObjectName(u"infoLabel")
        self.infoLabel.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignTop)
        self.infoLabel.setWordWrap(True)
        self.infoLabel.setOpenExternalLinks(True)

        self.verticalLayout_2.addWidget(self.infoLabel)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_2.addItem(self.verticalSpacer_2)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.buttonBack2 = QPushButton(self.frameInfo)
        self.buttonBack2.setObjectName(u"buttonBack2")

        self.horizontalLayout_4.addWidget(self.buttonBack2)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_4.addItem(self.horizontalSpacer_2)


        self.verticalLayout_2.addLayout(self.horizontalLayout_4)


        self.horizontalLayout.addWidget(self.frameInfo)


        self.retranslateUi(Dialog)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Dialog", None))
        self.pitchLabel.setText(QCoreApplication.translate("Dialog", u"<html><head/><body><p>This app was made by one student in his spare time, and that takes a lot of work. If you like what I do, you can support me by giving me [amount of money here] one time, and I'll send you a key to get more themes, and the ability to set a name for your timetable to be associated with. If not, that's fine, you can continue to use this forever with no ads or other restrictions, but if you have cash to spare and want to show off all your sleek themes, please consider.</p><p>If you've already bought a key, use the 'upload key' button below to select it.</p></body></html>", None))
        self.buttonBack.setText(QCoreApplication.translate("Dialog", u"Back", None))
        self.buttonInfo.setText(QCoreApplication.translate("Dialog", u"How to buy", None))
        self.buttonLoad.setText(QCoreApplication.translate("Dialog", u"Upload key", None))
        self.infoLabel.setText(QCoreApplication.translate("Dialog", u"<html><head/><body><p>This is placeholder text. If you see this, please kindly remind Rafael to fix this and make real information on how to buy more themes.</p></body></html>", None))
        self.buttonBack2.setText(QCoreApplication.translate("Dialog", u"Back", None))
    # retranslateUi

