# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'custom.ui'
##
## Created by: Qt User Interface Compiler version 6.2.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QHBoxLayout, QLabel, QPushButton,
    QSizePolicy, QSpacerItem, QSpinBox, QVBoxLayout,
    QWidget)
import backgrounds_rc

class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.setWindowModality(Qt.ApplicationModal)
        Form.resize(419, 208)
        icon = QIcon()
        icon.addFile(u":/Images/timetable.png", QSize(), QIcon.Normal, QIcon.Off)
        Form.setWindowIcon(icon)
        self.verticalLayout = QVBoxLayout(Form)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.foregroundLayout = QHBoxLayout()
        self.foregroundLayout.setObjectName(u"foregroundLayout")
        self.foregroundLayout.setContentsMargins(15, -1, 20, -1)
        self.labelFore = QLabel(Form)
        self.labelFore.setObjectName(u"labelFore")
        self.labelFore.setMinimumSize(QSize(80, 0))
        self.labelFore.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.foregroundLayout.addWidget(self.labelFore)

        self.labelForeRGB = QLabel(Form)
        self.labelForeRGB.setObjectName(u"labelForeRGB")
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelForeRGB.sizePolicy().hasHeightForWidth())
        self.labelForeRGB.setSizePolicy(sizePolicy)

        self.foregroundLayout.addWidget(self.labelForeRGB)

        self.spinForeR = QSpinBox(Form)
        self.spinForeR.setObjectName(u"spinForeR")
        sizePolicy1 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.spinForeR.sizePolicy().hasHeightForWidth())
        self.spinForeR.setSizePolicy(sizePolicy1)
        self.spinForeR.setMaximum(255)

        self.foregroundLayout.addWidget(self.spinForeR)

        self.labelComma = QLabel(Form)
        self.labelComma.setObjectName(u"labelComma")
        sizePolicy.setHeightForWidth(self.labelComma.sizePolicy().hasHeightForWidth())
        self.labelComma.setSizePolicy(sizePolicy)

        self.foregroundLayout.addWidget(self.labelComma)

        self.spinForeG = QSpinBox(Form)
        self.spinForeG.setObjectName(u"spinForeG")
        sizePolicy1.setHeightForWidth(self.spinForeG.sizePolicy().hasHeightForWidth())
        self.spinForeG.setSizePolicy(sizePolicy1)
        self.spinForeG.setMaximum(255)

        self.foregroundLayout.addWidget(self.spinForeG)

        self.labelComma2 = QLabel(Form)
        self.labelComma2.setObjectName(u"labelComma2")
        sizePolicy.setHeightForWidth(self.labelComma2.sizePolicy().hasHeightForWidth())
        self.labelComma2.setSizePolicy(sizePolicy)

        self.foregroundLayout.addWidget(self.labelComma2)

        self.spinForeB = QSpinBox(Form)
        self.spinForeB.setObjectName(u"spinForeB")
        sizePolicy1.setHeightForWidth(self.spinForeB.sizePolicy().hasHeightForWidth())
        self.spinForeB.setSizePolicy(sizePolicy1)
        self.spinForeB.setMaximum(255)

        self.foregroundLayout.addWidget(self.spinForeB)

        self.labelForeBracket = QLabel(Form)
        self.labelForeBracket.setObjectName(u"labelForeBracket")
        sizePolicy.setHeightForWidth(self.labelForeBracket.sizePolicy().hasHeightForWidth())
        self.labelForeBracket.setSizePolicy(sizePolicy)

        self.foregroundLayout.addWidget(self.labelForeBracket)


        self.verticalLayout.addLayout(self.foregroundLayout)

        self.backgroundLayout = QHBoxLayout()
        self.backgroundLayout.setObjectName(u"backgroundLayout")
        self.backgroundLayout.setContentsMargins(15, -1, 20, -1)
        self.labelBack = QLabel(Form)
        self.labelBack.setObjectName(u"labelBack")
        self.labelBack.setMinimumSize(QSize(80, 0))
        self.labelBack.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.backgroundLayout.addWidget(self.labelBack)

        self.labelBackRGB = QLabel(Form)
        self.labelBackRGB.setObjectName(u"labelBackRGB")
        sizePolicy.setHeightForWidth(self.labelBackRGB.sizePolicy().hasHeightForWidth())
        self.labelBackRGB.setSizePolicy(sizePolicy)

        self.backgroundLayout.addWidget(self.labelBackRGB)

        self.spinBackR = QSpinBox(Form)
        self.spinBackR.setObjectName(u"spinBackR")
        sizePolicy1.setHeightForWidth(self.spinBackR.sizePolicy().hasHeightForWidth())
        self.spinBackR.setSizePolicy(sizePolicy1)
        self.spinBackR.setMaximum(255)

        self.backgroundLayout.addWidget(self.spinBackR)

        self.labelComma3 = QLabel(Form)
        self.labelComma3.setObjectName(u"labelComma3")
        sizePolicy.setHeightForWidth(self.labelComma3.sizePolicy().hasHeightForWidth())
        self.labelComma3.setSizePolicy(sizePolicy)

        self.backgroundLayout.addWidget(self.labelComma3)

        self.spinBackG = QSpinBox(Form)
        self.spinBackG.setObjectName(u"spinBackG")
        sizePolicy1.setHeightForWidth(self.spinBackG.sizePolicy().hasHeightForWidth())
        self.spinBackG.setSizePolicy(sizePolicy1)
        self.spinBackG.setMaximum(255)

        self.backgroundLayout.addWidget(self.spinBackG)

        self.labelComma4 = QLabel(Form)
        self.labelComma4.setObjectName(u"labelComma4")
        sizePolicy.setHeightForWidth(self.labelComma4.sizePolicy().hasHeightForWidth())
        self.labelComma4.setSizePolicy(sizePolicy)

        self.backgroundLayout.addWidget(self.labelComma4)

        self.spinBackB = QSpinBox(Form)
        self.spinBackB.setObjectName(u"spinBackB")
        sizePolicy1.setHeightForWidth(self.spinBackB.sizePolicy().hasHeightForWidth())
        self.spinBackB.setSizePolicy(sizePolicy1)
        self.spinBackB.setMaximum(255)

        self.backgroundLayout.addWidget(self.spinBackB)

        self.labelBackBracket = QLabel(Form)
        self.labelBackBracket.setObjectName(u"labelBackBracket")
        sizePolicy.setHeightForWidth(self.labelBackBracket.sizePolicy().hasHeightForWidth())
        self.labelBackBracket.setSizePolicy(sizePolicy)

        self.backgroundLayout.addWidget(self.labelBackBracket)


        self.verticalLayout.addLayout(self.backgroundLayout)

        self.backgroundAltLayout = QHBoxLayout()
        self.backgroundAltLayout.setObjectName(u"backgroundAltLayout")
        self.backgroundAltLayout.setContentsMargins(15, -1, 20, -1)
        self.labelBackAlt = QLabel(Form)
        self.labelBackAlt.setObjectName(u"labelBackAlt")
        self.labelBackAlt.setMinimumSize(QSize(80, 0))
        self.labelBackAlt.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.backgroundAltLayout.addWidget(self.labelBackAlt)

        self.labelBackAltRGB = QLabel(Form)
        self.labelBackAltRGB.setObjectName(u"labelBackAltRGB")
        sizePolicy.setHeightForWidth(self.labelBackAltRGB.sizePolicy().hasHeightForWidth())
        self.labelBackAltRGB.setSizePolicy(sizePolicy)

        self.backgroundAltLayout.addWidget(self.labelBackAltRGB)

        self.spinBackAltR = QSpinBox(Form)
        self.spinBackAltR.setObjectName(u"spinBackAltR")
        sizePolicy1.setHeightForWidth(self.spinBackAltR.sizePolicy().hasHeightForWidth())
        self.spinBackAltR.setSizePolicy(sizePolicy1)
        self.spinBackAltR.setMaximum(255)

        self.backgroundAltLayout.addWidget(self.spinBackAltR)

        self.labelComma5 = QLabel(Form)
        self.labelComma5.setObjectName(u"labelComma5")
        sizePolicy.setHeightForWidth(self.labelComma5.sizePolicy().hasHeightForWidth())
        self.labelComma5.setSizePolicy(sizePolicy)

        self.backgroundAltLayout.addWidget(self.labelComma5)

        self.spinBackAltG = QSpinBox(Form)
        self.spinBackAltG.setObjectName(u"spinBackAltG")
        sizePolicy1.setHeightForWidth(self.spinBackAltG.sizePolicy().hasHeightForWidth())
        self.spinBackAltG.setSizePolicy(sizePolicy1)
        self.spinBackAltG.setMaximum(255)

        self.backgroundAltLayout.addWidget(self.spinBackAltG)

        self.labelComma6 = QLabel(Form)
        self.labelComma6.setObjectName(u"labelComma6")
        sizePolicy.setHeightForWidth(self.labelComma6.sizePolicy().hasHeightForWidth())
        self.labelComma6.setSizePolicy(sizePolicy)

        self.backgroundAltLayout.addWidget(self.labelComma6)

        self.spinBackAltB = QSpinBox(Form)
        self.spinBackAltB.setObjectName(u"spinBackAltB")
        sizePolicy1.setHeightForWidth(self.spinBackAltB.sizePolicy().hasHeightForWidth())
        self.spinBackAltB.setSizePolicy(sizePolicy1)
        self.spinBackAltB.setMaximum(255)

        self.backgroundAltLayout.addWidget(self.spinBackAltB)

        self.labelBackAltBracket = QLabel(Form)
        self.labelBackAltBracket.setObjectName(u"labelBackAltBracket")
        sizePolicy.setHeightForWidth(self.labelBackAltBracket.sizePolicy().hasHeightForWidth())
        self.labelBackAltBracket.setSizePolicy(sizePolicy)

        self.backgroundAltLayout.addWidget(self.labelBackAltBracket)


        self.verticalLayout.addLayout(self.backgroundAltLayout)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_4.addItem(self.horizontalSpacer)

        self.buttonSave = QPushButton(Form)
        self.buttonSave.setObjectName(u"buttonSave")

        self.horizontalLayout_4.addWidget(self.buttonSave)


        self.verticalLayout.addLayout(self.horizontalLayout_4)


        self.retranslateUi(Form)

        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Form", None))
        self.labelFore.setText(QCoreApplication.translate("Form", u"Foreground:", None))
        self.labelForeRGB.setText(QCoreApplication.translate("Form", u"rgb(", None))
        self.labelComma.setText(QCoreApplication.translate("Form", u",", None))
        self.labelComma2.setText(QCoreApplication.translate("Form", u",", None))
        self.labelForeBracket.setText(QCoreApplication.translate("Form", u")", None))
        self.labelBack.setText(QCoreApplication.translate("Form", u"Background:", None))
        self.labelBackRGB.setText(QCoreApplication.translate("Form", u"rgb(", None))
        self.labelComma3.setText(QCoreApplication.translate("Form", u",", None))
        self.labelComma4.setText(QCoreApplication.translate("Form", u",", None))
        self.labelBackBracket.setText(QCoreApplication.translate("Form", u")", None))
        self.labelBackAlt.setText(QCoreApplication.translate("Form", u"Alternate:", None))
        self.labelBackAltRGB.setText(QCoreApplication.translate("Form", u"rgb(", None))
        self.labelComma5.setText(QCoreApplication.translate("Form", u",", None))
        self.labelComma6.setText(QCoreApplication.translate("Form", u",", None))
        self.labelBackAltBracket.setText(QCoreApplication.translate("Form", u")", None))
        self.buttonSave.setText(QCoreApplication.translate("Form", u"Ok", None))
    # retranslateUi

