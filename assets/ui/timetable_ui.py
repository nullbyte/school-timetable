# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'timetable.ui'
##
## Created by: Qt User Interface Compiler version 6.2.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QFrame, QHBoxLayout, QLabel,
    QMainWindow, QPushButton, QSizePolicy, QSpacerItem,
    QVBoxLayout, QWidget)
import backgrounds_rc

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(617, 535)
        MainWindow.setAutoFillBackground(False)
        MainWindow.setStyleSheet(u"QWidget {\n"
"	background-color: rgb(34, 34, 34);\n"
"}\n"
"QLabel {\n"
"	color: rgb(255, 255, 255);\n"
"}")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.centralwidget.setStyleSheet(u"")
        self.verticalLayout = QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.header = QFrame(self.centralwidget)
        self.header.setObjectName(u"header")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.header.sizePolicy().hasHeightForWidth())
        self.header.setSizePolicy(sizePolicy)
        self.header.setMinimumSize(QSize(0, 0))
        self.header.setMaximumSize(QSize(16777215, 40))
        self.header.setFrameShape(QFrame.NoFrame)
        self.header.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_12 = QHBoxLayout(self.header)
        self.horizontalLayout_12.setObjectName(u"horizontalLayout_12")
        self.labelTitle = QLabel(self.header)
        self.labelTitle.setObjectName(u"labelTitle")
        sizePolicy.setHeightForWidth(self.labelTitle.sizePolicy().hasHeightForWidth())
        self.labelTitle.setSizePolicy(sizePolicy)
        self.labelTitle.setStyleSheet(u"font: 18pt \"Google Sans\";")

        self.horizontalLayout_12.addWidget(self.labelTitle)

        self.headerSpacer = QFrame(self.header)
        self.headerSpacer.setObjectName(u"headerSpacer")
        sizePolicy1 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.headerSpacer.sizePolicy().hasHeightForWidth())
        self.headerSpacer.setSizePolicy(sizePolicy1)
        self.headerSpacer.setFrameShape(QFrame.NoFrame)
        self.headerSpacer.setFrameShadow(QFrame.Raised)

        self.horizontalLayout_12.addWidget(self.headerSpacer)

        self.buttonMinimize = QPushButton(self.header)
        self.buttonMinimize.setObjectName(u"buttonMinimize")
        sizePolicy2 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.buttonMinimize.sizePolicy().hasHeightForWidth())
        self.buttonMinimize.setSizePolicy(sizePolicy2)
        icon = QIcon()
        iconThemeName = u"window-minimize"
        if QIcon.hasThemeIcon(iconThemeName):
            icon = QIcon.fromTheme(iconThemeName)
        else:
            icon.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)
        
        self.buttonMinimize.setIcon(icon)
        self.buttonMinimize.setFlat(True)

        self.horizontalLayout_12.addWidget(self.buttonMinimize)

        self.buttonMaximize = QPushButton(self.header)
        self.buttonMaximize.setObjectName(u"buttonMaximize")
        sizePolicy2.setHeightForWidth(self.buttonMaximize.sizePolicy().hasHeightForWidth())
        self.buttonMaximize.setSizePolicy(sizePolicy2)
        icon1 = QIcon()
        iconThemeName = u"window-maximize"
        if QIcon.hasThemeIcon(iconThemeName):
            icon1 = QIcon.fromTheme(iconThemeName)
        else:
            icon1.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)
        
        self.buttonMaximize.setIcon(icon1)
        self.buttonMaximize.setFlat(True)

        self.horizontalLayout_12.addWidget(self.buttonMaximize)

        self.buttonClose = QPushButton(self.header)
        self.buttonClose.setObjectName(u"buttonClose")
        sizePolicy2.setHeightForWidth(self.buttonClose.sizePolicy().hasHeightForWidth())
        self.buttonClose.setSizePolicy(sizePolicy2)
        icon2 = QIcon()
        iconThemeName = u"window-close"
        if QIcon.hasThemeIcon(iconThemeName):
            icon2 = QIcon.fromTheme(iconThemeName)
        else:
            icon2.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)
        
        self.buttonClose.setIcon(icon2)
        self.buttonClose.setFlat(True)

        self.horizontalLayout_12.addWidget(self.buttonClose)


        self.verticalLayout.addWidget(self.header)

        self.dayheader = QHBoxLayout()
        self.dayheader.setObjectName(u"dayheader")
        self.buttonSettings = QPushButton(self.centralwidget)
        self.buttonSettings.setObjectName(u"buttonSettings")
        sizePolicy3 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.buttonSettings.sizePolicy().hasHeightForWidth())
        self.buttonSettings.setSizePolicy(sizePolicy3)
        self.buttonSettings.setMinimumSize(QSize(60, 0))
        self.buttonSettings.setMaximumSize(QSize(16777215, 16777215))
        self.buttonSettings.setCursor(QCursor(Qt.PointingHandCursor))
        icon3 = QIcon()
        iconThemeName = u"settings"
        if QIcon.hasThemeIcon(iconThemeName):
            icon3 = QIcon.fromTheme(iconThemeName)
        else:
            icon3.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)
        
        self.buttonSettings.setIcon(icon3)
        self.buttonSettings.setFlat(True)

        self.dayheader.addWidget(self.buttonSettings)

        self.labelMonday = QLabel(self.centralwidget)
        self.labelMonday.setObjectName(u"labelMonday")
        sizePolicy3.setHeightForWidth(self.labelMonday.sizePolicy().hasHeightForWidth())
        self.labelMonday.setSizePolicy(sizePolicy3)
        self.labelMonday.setAlignment(Qt.AlignCenter)

        self.dayheader.addWidget(self.labelMonday)

        self.labelTuesday = QLabel(self.centralwidget)
        self.labelTuesday.setObjectName(u"labelTuesday")
        sizePolicy3.setHeightForWidth(self.labelTuesday.sizePolicy().hasHeightForWidth())
        self.labelTuesday.setSizePolicy(sizePolicy3)
        self.labelTuesday.setAlignment(Qt.AlignCenter)

        self.dayheader.addWidget(self.labelTuesday)

        self.labelWednesday = QLabel(self.centralwidget)
        self.labelWednesday.setObjectName(u"labelWednesday")
        sizePolicy3.setHeightForWidth(self.labelWednesday.sizePolicy().hasHeightForWidth())
        self.labelWednesday.setSizePolicy(sizePolicy3)
        self.labelWednesday.setScaledContents(False)
        self.labelWednesday.setAlignment(Qt.AlignCenter)

        self.dayheader.addWidget(self.labelWednesday)

        self.labelThursday = QLabel(self.centralwidget)
        self.labelThursday.setObjectName(u"labelThursday")
        sizePolicy3.setHeightForWidth(self.labelThursday.sizePolicy().hasHeightForWidth())
        self.labelThursday.setSizePolicy(sizePolicy3)
        self.labelThursday.setAlignment(Qt.AlignCenter)

        self.dayheader.addWidget(self.labelThursday)

        self.labelFriday = QLabel(self.centralwidget)
        self.labelFriday.setObjectName(u"labelFriday")
        sizePolicy3.setHeightForWidth(self.labelFriday.sizePolicy().hasHeightForWidth())
        self.labelFriday.setSizePolicy(sizePolicy3)
        self.labelFriday.setAlignment(Qt.AlignCenter)

        self.dayheader.addWidget(self.labelFriday)

        self.dayheader.setStretch(0, 1)
        self.dayheader.setStretch(1, 2)
        self.dayheader.setStretch(2, 2)
        self.dayheader.setStretch(3, 2)
        self.dayheader.setStretch(4, 2)
        self.dayheader.setStretch(5, 2)

        self.verticalLayout.addLayout(self.dayheader)

        self.lesson1to2 = QHBoxLayout()
        self.lesson1to2.setObjectName(u"lesson1to2")
        self.time1to2 = QVBoxLayout()
        self.time1to2.setObjectName(u"time1to2")
        self.labelTime1_2 = QLabel(self.centralwidget)
        self.labelTime1_2.setObjectName(u"labelTime1_2")
        self.labelTime1_2.setMinimumSize(QSize(60, 0))
        self.labelTime1_2.setMaximumSize(QSize(16777215, 16777215))
        self.labelTime1_2.setAlignment(Qt.AlignCenter)

        self.time1to2.addWidget(self.labelTime1_2)

        self.labelTime2_2 = QLabel(self.centralwidget)
        self.labelTime2_2.setObjectName(u"labelTime2_2")
        self.labelTime2_2.setMinimumSize(QSize(60, 0))
        self.labelTime2_2.setMaximumSize(QSize(16777215, 16777215))
        self.labelTime2_2.setAlignment(Qt.AlignCenter)

        self.time1to2.addWidget(self.labelTime2_2)


        self.lesson1to2.addLayout(self.time1to2)

        self.monday1to2 = QVBoxLayout()
        self.monday1to2.setObjectName(u"monday1to2")
        self.monday1 = QPushButton(self.centralwidget)
        self.monday1.setObjectName(u"monday1")
        sizePolicy4 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy4.setHorizontalStretch(0)
        sizePolicy4.setVerticalStretch(1)
        sizePolicy4.setHeightForWidth(self.monday1.sizePolicy().hasHeightForWidth())
        self.monday1.setSizePolicy(sizePolicy4)
        self.monday1.setMinimumSize(QSize(90, 0))
        self.monday1.setCursor(QCursor(Qt.PointingHandCursor))
        self.monday1.setStyleSheet(u"")

        self.monday1to2.addWidget(self.monday1)

        self.monday2 = QPushButton(self.centralwidget)
        self.monday2.setObjectName(u"monday2")
        sizePolicy4.setHeightForWidth(self.monday2.sizePolicy().hasHeightForWidth())
        self.monday2.setSizePolicy(sizePolicy4)
        self.monday2.setMinimumSize(QSize(90, 0))
        self.monday2.setCursor(QCursor(Qt.PointingHandCursor))
        self.monday2.setStyleSheet(u"")

        self.monday1to2.addWidget(self.monday2)


        self.lesson1to2.addLayout(self.monday1to2)

        self.tuesday1to2 = QVBoxLayout()
        self.tuesday1to2.setObjectName(u"tuesday1to2")
        self.tuesday1 = QPushButton(self.centralwidget)
        self.tuesday1.setObjectName(u"tuesday1")
        sizePolicy4.setHeightForWidth(self.tuesday1.sizePolicy().hasHeightForWidth())
        self.tuesday1.setSizePolicy(sizePolicy4)
        self.tuesday1.setMinimumSize(QSize(90, 0))
        self.tuesday1.setCursor(QCursor(Qt.PointingHandCursor))
        self.tuesday1.setStyleSheet(u"")

        self.tuesday1to2.addWidget(self.tuesday1)

        self.tuesday2 = QPushButton(self.centralwidget)
        self.tuesday2.setObjectName(u"tuesday2")
        sizePolicy4.setHeightForWidth(self.tuesday2.sizePolicy().hasHeightForWidth())
        self.tuesday2.setSizePolicy(sizePolicy4)
        self.tuesday2.setMinimumSize(QSize(90, 0))
        self.tuesday2.setCursor(QCursor(Qt.PointingHandCursor))
        self.tuesday2.setStyleSheet(u"")

        self.tuesday1to2.addWidget(self.tuesday2)


        self.lesson1to2.addLayout(self.tuesday1to2)

        self.wednesday1to2 = QVBoxLayout()
        self.wednesday1to2.setObjectName(u"wednesday1to2")
        self.wednesday1 = QPushButton(self.centralwidget)
        self.wednesday1.setObjectName(u"wednesday1")
        sizePolicy4.setHeightForWidth(self.wednesday1.sizePolicy().hasHeightForWidth())
        self.wednesday1.setSizePolicy(sizePolicy4)
        self.wednesday1.setMinimumSize(QSize(90, 0))
        self.wednesday1.setCursor(QCursor(Qt.PointingHandCursor))
        self.wednesday1.setStyleSheet(u"")

        self.wednesday1to2.addWidget(self.wednesday1)

        self.wednesday2 = QPushButton(self.centralwidget)
        self.wednesday2.setObjectName(u"wednesday2")
        sizePolicy4.setHeightForWidth(self.wednesday2.sizePolicy().hasHeightForWidth())
        self.wednesday2.setSizePolicy(sizePolicy4)
        self.wednesday2.setMinimumSize(QSize(90, 0))
        self.wednesday2.setCursor(QCursor(Qt.PointingHandCursor))
        self.wednesday2.setStyleSheet(u"")

        self.wednesday1to2.addWidget(self.wednesday2)


        self.lesson1to2.addLayout(self.wednesday1to2)

        self.thursday1to2 = QVBoxLayout()
        self.thursday1to2.setObjectName(u"thursday1to2")
        self.thursday1 = QPushButton(self.centralwidget)
        self.thursday1.setObjectName(u"thursday1")
        sizePolicy4.setHeightForWidth(self.thursday1.sizePolicy().hasHeightForWidth())
        self.thursday1.setSizePolicy(sizePolicy4)
        self.thursday1.setMinimumSize(QSize(90, 0))
        self.thursday1.setCursor(QCursor(Qt.PointingHandCursor))
        self.thursday1.setStyleSheet(u"")

        self.thursday1to2.addWidget(self.thursday1)

        self.thursday2 = QPushButton(self.centralwidget)
        self.thursday2.setObjectName(u"thursday2")
        sizePolicy4.setHeightForWidth(self.thursday2.sizePolicy().hasHeightForWidth())
        self.thursday2.setSizePolicy(sizePolicy4)
        self.thursday2.setMinimumSize(QSize(90, 0))
        self.thursday2.setCursor(QCursor(Qt.PointingHandCursor))
        self.thursday2.setStyleSheet(u"")

        self.thursday1to2.addWidget(self.thursday2)


        self.lesson1to2.addLayout(self.thursday1to2)

        self.friday1to2 = QVBoxLayout()
        self.friday1to2.setObjectName(u"friday1to2")
        self.friday1 = QPushButton(self.centralwidget)
        self.friday1.setObjectName(u"friday1")
        sizePolicy4.setHeightForWidth(self.friday1.sizePolicy().hasHeightForWidth())
        self.friday1.setSizePolicy(sizePolicy4)
        self.friday1.setMinimumSize(QSize(90, 0))
        self.friday1.setCursor(QCursor(Qt.PointingHandCursor))
        self.friday1.setStyleSheet(u"")

        self.friday1to2.addWidget(self.friday1)

        self.friday2 = QPushButton(self.centralwidget)
        self.friday2.setObjectName(u"friday2")
        sizePolicy4.setHeightForWidth(self.friday2.sizePolicy().hasHeightForWidth())
        self.friday2.setSizePolicy(sizePolicy4)
        self.friday2.setMinimumSize(QSize(90, 0))
        self.friday2.setCursor(QCursor(Qt.PointingHandCursor))
        self.friday2.setStyleSheet(u"")

        self.friday1to2.addWidget(self.friday2)


        self.lesson1to2.addLayout(self.friday1to2)

        self.lesson1to2.setStretch(0, 1)
        self.lesson1to2.setStretch(1, 2)
        self.lesson1to2.setStretch(2, 2)
        self.lesson1to2.setStretch(3, 2)
        self.lesson1to2.setStretch(4, 2)
        self.lesson1to2.setStretch(5, 2)

        self.verticalLayout.addLayout(self.lesson1to2)

        self.recess = QHBoxLayout()
        self.recess.setObjectName(u"recess")
        self.labelTimeRecess = QLabel(self.centralwidget)
        self.labelTimeRecess.setObjectName(u"labelTimeRecess")
        self.labelTimeRecess.setMinimumSize(QSize(60, 0))
        self.labelTimeRecess.setMaximumSize(QSize(16777215, 16777215))
        self.labelTimeRecess.setAlignment(Qt.AlignCenter)

        self.recess.addWidget(self.labelTimeRecess)

        self.labelRecess = QLabel(self.centralwidget)
        self.labelRecess.setObjectName(u"labelRecess")
        sizePolicy3.setHeightForWidth(self.labelRecess.sizePolicy().hasHeightForWidth())
        self.labelRecess.setSizePolicy(sizePolicy3)
        self.labelRecess.setAlignment(Qt.AlignCenter)

        self.recess.addWidget(self.labelRecess)

        self.recess.setStretch(0, 1)
        self.recess.setStretch(1, 10)

        self.verticalLayout.addLayout(self.recess)

        self.lesson3to5 = QHBoxLayout()
        self.lesson3to5.setObjectName(u"lesson3to5")
        self.time3to5 = QVBoxLayout()
        self.time3to5.setObjectName(u"time3to5")
        self.labelTime3 = QLabel(self.centralwidget)
        self.labelTime3.setObjectName(u"labelTime3")
        sizePolicy3.setHeightForWidth(self.labelTime3.sizePolicy().hasHeightForWidth())
        self.labelTime3.setSizePolicy(sizePolicy3)
        self.labelTime3.setMinimumSize(QSize(60, 0))
        self.labelTime3.setMaximumSize(QSize(16777215, 16777215))
        self.labelTime3.setAlignment(Qt.AlignCenter)

        self.time3to5.addWidget(self.labelTime3)

        self.labelTime4 = QLabel(self.centralwidget)
        self.labelTime4.setObjectName(u"labelTime4")
        sizePolicy3.setHeightForWidth(self.labelTime4.sizePolicy().hasHeightForWidth())
        self.labelTime4.setSizePolicy(sizePolicy3)
        self.labelTime4.setMinimumSize(QSize(60, 0))
        self.labelTime4.setMaximumSize(QSize(16777215, 16777215))
        self.labelTime4.setAlignment(Qt.AlignCenter)

        self.time3to5.addWidget(self.labelTime4)

        self.labelTime5 = QLabel(self.centralwidget)
        self.labelTime5.setObjectName(u"labelTime5")
        sizePolicy3.setHeightForWidth(self.labelTime5.sizePolicy().hasHeightForWidth())
        self.labelTime5.setSizePolicy(sizePolicy3)
        self.labelTime5.setMinimumSize(QSize(60, 0))
        self.labelTime5.setMaximumSize(QSize(16777215, 16777215))
        self.labelTime5.setAlignment(Qt.AlignCenter)

        self.time3to5.addWidget(self.labelTime5)


        self.lesson3to5.addLayout(self.time3to5)

        self.monday3to5 = QVBoxLayout()
        self.monday3to5.setObjectName(u"monday3to5")
        self.monday3 = QPushButton(self.centralwidget)
        self.monday3.setObjectName(u"monday3")
        sizePolicy4.setHeightForWidth(self.monday3.sizePolicy().hasHeightForWidth())
        self.monday3.setSizePolicy(sizePolicy4)
        self.monday3.setMinimumSize(QSize(90, 0))
        self.monday3.setCursor(QCursor(Qt.PointingHandCursor))
        self.monday3.setStyleSheet(u"")

        self.monday3to5.addWidget(self.monday3)

        self.monday4 = QPushButton(self.centralwidget)
        self.monday4.setObjectName(u"monday4")
        sizePolicy4.setHeightForWidth(self.monday4.sizePolicy().hasHeightForWidth())
        self.monday4.setSizePolicy(sizePolicy4)
        self.monday4.setMinimumSize(QSize(90, 0))
        self.monday4.setCursor(QCursor(Qt.PointingHandCursor))
        self.monday4.setStyleSheet(u"")

        self.monday3to5.addWidget(self.monday4)

        self.monday5 = QPushButton(self.centralwidget)
        self.monday5.setObjectName(u"monday5")
        sizePolicy4.setHeightForWidth(self.monday5.sizePolicy().hasHeightForWidth())
        self.monday5.setSizePolicy(sizePolicy4)
        self.monday5.setMinimumSize(QSize(90, 0))
        self.monday5.setCursor(QCursor(Qt.PointingHandCursor))
        self.monday5.setStyleSheet(u"")

        self.monday3to5.addWidget(self.monday5)


        self.lesson3to5.addLayout(self.monday3to5)

        self.tuesday3to5 = QVBoxLayout()
        self.tuesday3to5.setObjectName(u"tuesday3to5")
        self.tuesday3 = QPushButton(self.centralwidget)
        self.tuesday3.setObjectName(u"tuesday3")
        sizePolicy4.setHeightForWidth(self.tuesday3.sizePolicy().hasHeightForWidth())
        self.tuesday3.setSizePolicy(sizePolicy4)
        self.tuesday3.setMinimumSize(QSize(90, 0))
        self.tuesday3.setCursor(QCursor(Qt.PointingHandCursor))
        self.tuesday3.setStyleSheet(u"")

        self.tuesday3to5.addWidget(self.tuesday3)

        self.tuesday4 = QPushButton(self.centralwidget)
        self.tuesday4.setObjectName(u"tuesday4")
        sizePolicy4.setHeightForWidth(self.tuesday4.sizePolicy().hasHeightForWidth())
        self.tuesday4.setSizePolicy(sizePolicy4)
        self.tuesday4.setMinimumSize(QSize(90, 0))
        self.tuesday4.setCursor(QCursor(Qt.PointingHandCursor))
        self.tuesday4.setStyleSheet(u"")

        self.tuesday3to5.addWidget(self.tuesday4)

        self.tuesday5 = QPushButton(self.centralwidget)
        self.tuesday5.setObjectName(u"tuesday5")
        sizePolicy4.setHeightForWidth(self.tuesday5.sizePolicy().hasHeightForWidth())
        self.tuesday5.setSizePolicy(sizePolicy4)
        self.tuesday5.setMinimumSize(QSize(90, 0))
        self.tuesday5.setCursor(QCursor(Qt.PointingHandCursor))
        self.tuesday5.setStyleSheet(u"")

        self.tuesday3to5.addWidget(self.tuesday5)


        self.lesson3to5.addLayout(self.tuesday3to5)

        self.wednesday3to5 = QVBoxLayout()
        self.wednesday3to5.setObjectName(u"wednesday3to5")
        self.wednesday3 = QPushButton(self.centralwidget)
        self.wednesday3.setObjectName(u"wednesday3")
        sizePolicy4.setHeightForWidth(self.wednesday3.sizePolicy().hasHeightForWidth())
        self.wednesday3.setSizePolicy(sizePolicy4)
        self.wednesday3.setMinimumSize(QSize(90, 0))
        self.wednesday3.setCursor(QCursor(Qt.PointingHandCursor))
        self.wednesday3.setStyleSheet(u"")

        self.wednesday3to5.addWidget(self.wednesday3)

        self.wednesday4 = QPushButton(self.centralwidget)
        self.wednesday4.setObjectName(u"wednesday4")
        sizePolicy4.setHeightForWidth(self.wednesday4.sizePolicy().hasHeightForWidth())
        self.wednesday4.setSizePolicy(sizePolicy4)
        self.wednesday4.setMinimumSize(QSize(90, 0))
        self.wednesday4.setCursor(QCursor(Qt.PointingHandCursor))
        self.wednesday4.setStyleSheet(u"")

        self.wednesday3to5.addWidget(self.wednesday4)

        self.wednesday5 = QPushButton(self.centralwidget)
        self.wednesday5.setObjectName(u"wednesday5")
        sizePolicy4.setHeightForWidth(self.wednesday5.sizePolicy().hasHeightForWidth())
        self.wednesday5.setSizePolicy(sizePolicy4)
        self.wednesday5.setMinimumSize(QSize(90, 0))
        self.wednesday5.setCursor(QCursor(Qt.PointingHandCursor))
        self.wednesday5.setStyleSheet(u"")

        self.wednesday3to5.addWidget(self.wednesday5)


        self.lesson3to5.addLayout(self.wednesday3to5)

        self.thursday3to5 = QVBoxLayout()
        self.thursday3to5.setObjectName(u"thursday3to5")
        self.thursday3 = QPushButton(self.centralwidget)
        self.thursday3.setObjectName(u"thursday3")
        sizePolicy4.setHeightForWidth(self.thursday3.sizePolicy().hasHeightForWidth())
        self.thursday3.setSizePolicy(sizePolicy4)
        self.thursday3.setMinimumSize(QSize(90, 0))
        self.thursday3.setCursor(QCursor(Qt.PointingHandCursor))
        self.thursday3.setStyleSheet(u"")

        self.thursday3to5.addWidget(self.thursday3)

        self.thursday4 = QPushButton(self.centralwidget)
        self.thursday4.setObjectName(u"thursday4")
        sizePolicy4.setHeightForWidth(self.thursday4.sizePolicy().hasHeightForWidth())
        self.thursday4.setSizePolicy(sizePolicy4)
        self.thursday4.setMinimumSize(QSize(90, 0))
        self.thursday4.setCursor(QCursor(Qt.PointingHandCursor))
        self.thursday4.setStyleSheet(u"")

        self.thursday3to5.addWidget(self.thursday4)

        self.thursday5 = QPushButton(self.centralwidget)
        self.thursday5.setObjectName(u"thursday5")
        sizePolicy4.setHeightForWidth(self.thursday5.sizePolicy().hasHeightForWidth())
        self.thursday5.setSizePolicy(sizePolicy4)
        self.thursday5.setMinimumSize(QSize(90, 0))
        self.thursday5.setCursor(QCursor(Qt.PointingHandCursor))
        self.thursday5.setStyleSheet(u"")

        self.thursday3to5.addWidget(self.thursday5)


        self.lesson3to5.addLayout(self.thursday3to5)

        self.friday3to5 = QVBoxLayout()
        self.friday3to5.setObjectName(u"friday3to5")
        self.friday3 = QPushButton(self.centralwidget)
        self.friday3.setObjectName(u"friday3")
        sizePolicy4.setHeightForWidth(self.friday3.sizePolicy().hasHeightForWidth())
        self.friday3.setSizePolicy(sizePolicy4)
        self.friday3.setMinimumSize(QSize(90, 0))
        self.friday3.setCursor(QCursor(Qt.PointingHandCursor))
        self.friday3.setStyleSheet(u"")

        self.friday3to5.addWidget(self.friday3)

        self.friday4 = QPushButton(self.centralwidget)
        self.friday4.setObjectName(u"friday4")
        sizePolicy4.setHeightForWidth(self.friday4.sizePolicy().hasHeightForWidth())
        self.friday4.setSizePolicy(sizePolicy4)
        self.friday4.setMinimumSize(QSize(90, 0))
        self.friday4.setCursor(QCursor(Qt.PointingHandCursor))
        self.friday4.setStyleSheet(u"")

        self.friday3to5.addWidget(self.friday4)

        self.friday5 = QPushButton(self.centralwidget)
        self.friday5.setObjectName(u"friday5")
        sizePolicy4.setHeightForWidth(self.friday5.sizePolicy().hasHeightForWidth())
        self.friday5.setSizePolicy(sizePolicy4)
        self.friday5.setMinimumSize(QSize(90, 0))
        self.friday5.setCursor(QCursor(Qt.PointingHandCursor))
        self.friday5.setStyleSheet(u"")

        self.friday3to5.addWidget(self.friday5)


        self.lesson3to5.addLayout(self.friday3to5)

        self.lesson3to5.setStretch(0, 1)
        self.lesson3to5.setStretch(1, 2)
        self.lesson3to5.setStretch(2, 2)
        self.lesson3to5.setStretch(3, 2)
        self.lesson3to5.setStretch(4, 2)
        self.lesson3to5.setStretch(5, 2)

        self.verticalLayout.addLayout(self.lesson3to5)

        self.lunch = QHBoxLayout()
        self.lunch.setObjectName(u"lunch")
        self.labelTimeLunch = QLabel(self.centralwidget)
        self.labelTimeLunch.setObjectName(u"labelTimeLunch")
        self.labelTimeLunch.setMinimumSize(QSize(60, 0))
        self.labelTimeLunch.setMaximumSize(QSize(16777215, 16777215))
        self.labelTimeLunch.setAlignment(Qt.AlignCenter)

        self.lunch.addWidget(self.labelTimeLunch)

        self.labelLunch = QLabel(self.centralwidget)
        self.labelLunch.setObjectName(u"labelLunch")
        sizePolicy3.setHeightForWidth(self.labelLunch.sizePolicy().hasHeightForWidth())
        self.labelLunch.setSizePolicy(sizePolicy3)
        self.labelLunch.setAlignment(Qt.AlignCenter)

        self.lunch.addWidget(self.labelLunch)

        self.lunch.setStretch(0, 1)
        self.lunch.setStretch(1, 10)

        self.verticalLayout.addLayout(self.lunch)

        self.lesson6to7 = QHBoxLayout()
        self.lesson6to7.setObjectName(u"lesson6to7")
        self.time6to7 = QVBoxLayout()
        self.time6to7.setObjectName(u"time6to7")
        self.labelTime6 = QLabel(self.centralwidget)
        self.labelTime6.setObjectName(u"labelTime6")
        self.labelTime6.setMinimumSize(QSize(60, 0))
        self.labelTime6.setMaximumSize(QSize(16777215, 16777215))
        self.labelTime6.setAlignment(Qt.AlignCenter)

        self.time6to7.addWidget(self.labelTime6)

        self.labelTime7 = QLabel(self.centralwidget)
        self.labelTime7.setObjectName(u"labelTime7")
        self.labelTime7.setMinimumSize(QSize(60, 0))
        self.labelTime7.setMaximumSize(QSize(16777215, 16777215))
        self.labelTime7.setAlignment(Qt.AlignCenter)

        self.time6to7.addWidget(self.labelTime7)


        self.lesson6to7.addLayout(self.time6to7)

        self.monday6to7 = QVBoxLayout()
        self.monday6to7.setObjectName(u"monday6to7")
        self.monday6 = QPushButton(self.centralwidget)
        self.monday6.setObjectName(u"monday6")
        sizePolicy4.setHeightForWidth(self.monday6.sizePolicy().hasHeightForWidth())
        self.monday6.setSizePolicy(sizePolicy4)
        self.monday6.setMinimumSize(QSize(90, 0))
        self.monday6.setCursor(QCursor(Qt.PointingHandCursor))
        self.monday6.setStyleSheet(u"")

        self.monday6to7.addWidget(self.monday6)

        self.monday7 = QPushButton(self.centralwidget)
        self.monday7.setObjectName(u"monday7")
        sizePolicy4.setHeightForWidth(self.monday7.sizePolicy().hasHeightForWidth())
        self.monday7.setSizePolicy(sizePolicy4)
        self.monday7.setMinimumSize(QSize(90, 0))
        self.monday7.setCursor(QCursor(Qt.PointingHandCursor))
        self.monday7.setStyleSheet(u"")

        self.monday6to7.addWidget(self.monday7)


        self.lesson6to7.addLayout(self.monday6to7)

        self.tuesday6to7 = QVBoxLayout()
        self.tuesday6to7.setObjectName(u"tuesday6to7")
        self.tuesday6 = QPushButton(self.centralwidget)
        self.tuesday6.setObjectName(u"tuesday6")
        sizePolicy4.setHeightForWidth(self.tuesday6.sizePolicy().hasHeightForWidth())
        self.tuesday6.setSizePolicy(sizePolicy4)
        self.tuesday6.setMinimumSize(QSize(90, 0))
        self.tuesday6.setCursor(QCursor(Qt.PointingHandCursor))
        self.tuesday6.setStyleSheet(u"")

        self.tuesday6to7.addWidget(self.tuesday6)

        self.tuesday7 = QPushButton(self.centralwidget)
        self.tuesday7.setObjectName(u"tuesday7")
        sizePolicy4.setHeightForWidth(self.tuesday7.sizePolicy().hasHeightForWidth())
        self.tuesday7.setSizePolicy(sizePolicy4)
        self.tuesday7.setMinimumSize(QSize(90, 0))
        self.tuesday7.setCursor(QCursor(Qt.PointingHandCursor))
        self.tuesday7.setStyleSheet(u"")

        self.tuesday6to7.addWidget(self.tuesday7)


        self.lesson6to7.addLayout(self.tuesday6to7)

        self.wednesday6to7 = QVBoxLayout()
        self.wednesday6to7.setObjectName(u"wednesday6to7")
        self.wednesday6 = QPushButton(self.centralwidget)
        self.wednesday6.setObjectName(u"wednesday6")
        sizePolicy4.setHeightForWidth(self.wednesday6.sizePolicy().hasHeightForWidth())
        self.wednesday6.setSizePolicy(sizePolicy4)
        self.wednesday6.setMinimumSize(QSize(90, 0))
        self.wednesday6.setCursor(QCursor(Qt.PointingHandCursor))
        self.wednesday6.setStyleSheet(u"")

        self.wednesday6to7.addWidget(self.wednesday6)

        self.wednesday7 = QPushButton(self.centralwidget)
        self.wednesday7.setObjectName(u"wednesday7")
        sizePolicy4.setHeightForWidth(self.wednesday7.sizePolicy().hasHeightForWidth())
        self.wednesday7.setSizePolicy(sizePolicy4)
        self.wednesday7.setMinimumSize(QSize(90, 0))
        self.wednesday7.setCursor(QCursor(Qt.PointingHandCursor))
        self.wednesday7.setStyleSheet(u"")

        self.wednesday6to7.addWidget(self.wednesday7)


        self.lesson6to7.addLayout(self.wednesday6to7)

        self.thursday6to7 = QVBoxLayout()
        self.thursday6to7.setObjectName(u"thursday6to7")
        self.thursday6 = QPushButton(self.centralwidget)
        self.thursday6.setObjectName(u"thursday6")
        sizePolicy4.setHeightForWidth(self.thursday6.sizePolicy().hasHeightForWidth())
        self.thursday6.setSizePolicy(sizePolicy4)
        self.thursday6.setMinimumSize(QSize(90, 0))
        self.thursday6.setCursor(QCursor(Qt.PointingHandCursor))
        self.thursday6.setStyleSheet(u"")

        self.thursday6to7.addWidget(self.thursday6)

        self.thursday7 = QPushButton(self.centralwidget)
        self.thursday7.setObjectName(u"thursday7")
        sizePolicy4.setHeightForWidth(self.thursday7.sizePolicy().hasHeightForWidth())
        self.thursday7.setSizePolicy(sizePolicy4)
        self.thursday7.setMinimumSize(QSize(90, 0))
        self.thursday7.setCursor(QCursor(Qt.PointingHandCursor))
        self.thursday7.setStyleSheet(u"")

        self.thursday6to7.addWidget(self.thursday7)


        self.lesson6to7.addLayout(self.thursday6to7)

        self.friday6to7 = QVBoxLayout()
        self.friday6to7.setObjectName(u"friday6to7")
        self.friday6 = QPushButton(self.centralwidget)
        self.friday6.setObjectName(u"friday6")
        sizePolicy4.setHeightForWidth(self.friday6.sizePolicy().hasHeightForWidth())
        self.friday6.setSizePolicy(sizePolicy4)
        self.friday6.setMinimumSize(QSize(90, 0))
        self.friday6.setCursor(QCursor(Qt.PointingHandCursor))
        self.friday6.setStyleSheet(u"")

        self.friday6to7.addWidget(self.friday6)

        self.friday7 = QPushButton(self.centralwidget)
        self.friday7.setObjectName(u"friday7")
        sizePolicy4.setHeightForWidth(self.friday7.sizePolicy().hasHeightForWidth())
        self.friday7.setSizePolicy(sizePolicy4)
        self.friday7.setMinimumSize(QSize(90, 0))
        self.friday7.setCursor(QCursor(Qt.PointingHandCursor))
        self.friday7.setStyleSheet(u"")

        self.friday6to7.addWidget(self.friday7)


        self.lesson6to7.addLayout(self.friday6to7)

        self.lesson6to7.setStretch(0, 1)
        self.lesson6to7.setStretch(1, 2)
        self.lesson6to7.setStretch(2, 2)
        self.lesson6to7.setStretch(3, 2)
        self.lesson6to7.setStretch(4, 2)
        self.lesson6to7.setStretch(5, 2)

        self.verticalLayout.addLayout(self.lesson6to7)

        self.footer = QHBoxLayout()
        self.footer.setObjectName(u"footer")
        self.buttonAssignments = QPushButton(self.centralwidget)
        self.buttonAssignments.setObjectName(u"buttonAssignments")
        sizePolicy5 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        sizePolicy5.setHorizontalStretch(0)
        sizePolicy5.setVerticalStretch(0)
        sizePolicy5.setHeightForWidth(self.buttonAssignments.sizePolicy().hasHeightForWidth())
        self.buttonAssignments.setSizePolicy(sizePolicy5)
        self.buttonAssignments.setMinimumSize(QSize(130, 0))
        self.buttonAssignments.setMaximumSize(QSize(16777215, 30))

        self.footer.addWidget(self.buttonAssignments)

        self.buttonMap = QPushButton(self.centralwidget)
        self.buttonMap.setObjectName(u"buttonMap")
        self.buttonMap.setMaximumSize(QSize(16777215, 30))

        self.footer.addWidget(self.buttonMap)

        self.labelDate = QLabel(self.centralwidget)
        self.labelDate.setObjectName(u"labelDate")
        self.labelDate.setMaximumSize(QSize(16777215, 20))

        self.footer.addWidget(self.labelDate)

        self.footerSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.footer.addItem(self.footerSpacer)

        self.labelVersion = QLabel(self.centralwidget)
        self.labelVersion.setObjectName(u"labelVersion")
        self.labelVersion.setMaximumSize(QSize(16777215, 20))
        self.labelVersion.setStyleSheet(u"color: rgb(74, 74, 74);")

        self.footer.addWidget(self.labelVersion)


        self.verticalLayout.addLayout(self.footer)

        self.verticalLayout.setStretch(2, 2)
        self.verticalLayout.setStretch(4, 3)
        self.verticalLayout.setStretch(6, 2)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.labelTitle.setText(QCoreApplication.translate("MainWindow", u"<strong>Timetable", None))
        self.buttonMinimize.setText("")
        self.buttonMaximize.setText("")
        self.buttonClose.setText("")
        self.buttonSettings.setText("")
        self.labelMonday.setText(QCoreApplication.translate("MainWindow", u"<strong>Monday", None))
        self.labelTuesday.setText(QCoreApplication.translate("MainWindow", u"<strong>Tuesday", None))
        self.labelWednesday.setText(QCoreApplication.translate("MainWindow", u"<strong>Wednesday", None))
        self.labelThursday.setText(QCoreApplication.translate("MainWindow", u"<strong>Thursday", None))
        self.labelFriday.setText(QCoreApplication.translate("MainWindow", u"<strong>Friday", None))
        self.labelTime1_2.setText(QCoreApplication.translate("MainWindow", u"9:00", None))
        self.labelTime2_2.setText(QCoreApplication.translate("MainWindow", u"9:45", None))
        self.monday1.setText("")
        self.monday2.setText("")
        self.tuesday1.setText("")
        self.tuesday2.setText("")
        self.wednesday1.setText("")
        self.wednesday2.setText("")
        self.thursday1.setText("")
        self.thursday2.setText("")
        self.friday1.setText("")
        self.friday2.setText("")
        self.labelTimeRecess.setText(QCoreApplication.translate("MainWindow", u"10:30", None))
        self.labelRecess.setText(QCoreApplication.translate("MainWindow", u"Recess", None))
        self.labelTime3.setText(QCoreApplication.translate("MainWindow", u"10:50", None))
        self.labelTime4.setText(QCoreApplication.translate("MainWindow", u"11:35", None))
        self.labelTime5.setText(QCoreApplication.translate("MainWindow", u"12:20", None))
        self.monday3.setText("")
        self.monday4.setText("")
        self.monday5.setText("")
        self.tuesday3.setText("")
        self.tuesday4.setText("")
        self.tuesday5.setText("")
        self.wednesday3.setText("")
        self.wednesday4.setText("")
        self.wednesday5.setText("")
        self.thursday3.setText("")
        self.thursday4.setText("")
        self.thursday5.setText("")
        self.friday3.setText("")
        self.friday4.setText("")
        self.friday5.setText("")
        self.labelTimeLunch.setText(QCoreApplication.translate("MainWindow", u"1:05", None))
        self.labelLunch.setText(QCoreApplication.translate("MainWindow", u"Lunch", None))
        self.labelTime6.setText(QCoreApplication.translate("MainWindow", u"1:45", None))
        self.labelTime7.setText(QCoreApplication.translate("MainWindow", u"2:30", None))
        self.monday6.setText("")
        self.monday7.setText("")
        self.tuesday6.setText("")
        self.tuesday7.setText("")
        self.wednesday6.setText("")
        self.wednesday7.setText("")
        self.thursday6.setText("")
        self.thursday7.setText("")
        self.friday6.setText("")
        self.friday7.setText("")
        self.buttonAssignments.setText(QCoreApplication.translate("MainWindow", u"Assignments", None))
        self.buttonMap.setText(QCoreApplication.translate("MainWindow", u"School Map", None))
        self.labelDate.setText(QCoreApplication.translate("MainWindow", u"Unable to get current date", None))
        self.labelVersion.setText(QCoreApplication.translate("MainWindow", u"Unable to get current version", None))
    # retranslateUi

