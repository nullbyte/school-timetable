# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'assignment.ui'
##
## Created by: Qt User Interface Compiler version 6.2.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QHBoxLayout, QLabel, QPushButton,
    QSizePolicy, QSpacerItem, QVBoxLayout, QWidget)

class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(360, 120)
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Form.sizePolicy().hasHeightForWidth())
        Form.setSizePolicy(sizePolicy)
        Form.setMinimumSize(QSize(0, 120))
        Form.setMaximumSize(QSize(16777215, 120))
        self.horizontalLayout_2 = QHBoxLayout(Form)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.verticalLayout_2 = QVBoxLayout()
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.labelLesson = QLabel(Form)
        self.labelLesson.setObjectName(u"labelLesson")
        self.labelLesson.setStyleSheet(u"color: blue;")
        self.labelLesson.setAlignment(Qt.AlignBottom|Qt.AlignLeading|Qt.AlignLeft)

        self.horizontalLayout.addWidget(self.labelLesson)

        self.labelTitle = QLabel(Form)
        self.labelTitle.setObjectName(u"labelTitle")
        sizePolicy1 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.labelTitle.sizePolicy().hasHeightForWidth())
        self.labelTitle.setSizePolicy(sizePolicy1)
        self.labelTitle.setMaximumSize(QSize(225, 16777215))
        self.labelTitle.setAlignment(Qt.AlignBottom|Qt.AlignLeading|Qt.AlignLeft)
        self.labelTitle.setWordWrap(True)
        self.labelTitle.setMargin(2)

        self.horizontalLayout.addWidget(self.labelTitle)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)


        self.verticalLayout_2.addLayout(self.horizontalLayout)

        self.labelDesc = QLabel(Form)
        self.labelDesc.setObjectName(u"labelDesc")
        sizePolicy2 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.labelDesc.sizePolicy().hasHeightForWidth())
        self.labelDesc.setSizePolicy(sizePolicy2)
        self.labelDesc.setMaximumSize(QSize(305, 16777215))
        self.labelDesc.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignTop)
        self.labelDesc.setWordWrap(True)

        self.verticalLayout_2.addWidget(self.labelDesc)

        self.labelDate = QLabel(Form)
        self.labelDate.setObjectName(u"labelDate")

        self.verticalLayout_2.addWidget(self.labelDate)


        self.horizontalLayout_2.addLayout(self.verticalLayout_2)

        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setSpacing(5)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.buttonClose = QPushButton(Form)
        self.buttonClose.setObjectName(u"buttonClose")
        sizePolicy3 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.buttonClose.sizePolicy().hasHeightForWidth())
        self.buttonClose.setSizePolicy(sizePolicy3)
        self.buttonClose.setMaximumSize(QSize(30, 30))
        icon = QIcon()
        iconThemeName = u"view-close"
        if QIcon.hasThemeIcon(iconThemeName):
            icon = QIcon.fromTheme(iconThemeName)
        else:
            icon.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)
        
        self.buttonClose.setIcon(icon)

        self.verticalLayout.addWidget(self.buttonClose)

        self.buttonEdit = QPushButton(Form)
        self.buttonEdit.setObjectName(u"buttonEdit")
        sizePolicy3.setHeightForWidth(self.buttonEdit.sizePolicy().hasHeightForWidth())
        self.buttonEdit.setSizePolicy(sizePolicy3)
        self.buttonEdit.setMaximumSize(QSize(30, 30))
        icon1 = QIcon()
        iconThemeName = u"edit"
        if QIcon.hasThemeIcon(iconThemeName):
            icon1 = QIcon.fromTheme(iconThemeName)
        else:
            icon1.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)
        
        self.buttonEdit.setIcon(icon1)

        self.verticalLayout.addWidget(self.buttonEdit)

        self.buttonNotif = QPushButton(Form)
        self.buttonNotif.setObjectName(u"buttonNotif")
        self.buttonNotif.setMaximumSize(QSize(30, 30))
        icon2 = QIcon()
        iconThemeName = u"notifications-disabled"
        if QIcon.hasThemeIcon(iconThemeName):
            icon2 = QIcon.fromTheme(iconThemeName)
        else:
            icon2.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)
        
        self.buttonNotif.setIcon(icon2)

        self.verticalLayout.addWidget(self.buttonNotif)


        self.horizontalLayout_2.addLayout(self.verticalLayout)


        self.retranslateUi(Form)

        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Form", None))
        self.labelLesson.setText(QCoreApplication.translate("Form", u"<html><head/><body><p><span style=\" font-size:16pt; font-weight:600;\">Lesson</span></p></body></html>", None))
        self.labelTitle.setText(QCoreApplication.translate("Form", u"Name", None))
        self.labelDesc.setText(QCoreApplication.translate("Form", u"Description", None))
        self.labelDate.setText(QCoreApplication.translate("Form", u"Date", None))
        self.buttonClose.setText("")
        self.buttonEdit.setText("")
        self.buttonNotif.setText("")
    # retranslateUi

