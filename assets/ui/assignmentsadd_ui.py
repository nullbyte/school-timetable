# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'assignmentsadd.ui'
##
## Created by: Qt User Interface Compiler version 6.2.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QComboBox, QDateEdit, QFrame,
    QHBoxLayout, QLabel, QLineEdit, QPushButton,
    QSizePolicy, QSpacerItem, QTimeEdit, QVBoxLayout,
    QWidget)

class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.setWindowModality(Qt.ApplicationModal)
        Form.resize(404, 220)
        self.verticalLayout_2 = QVBoxLayout(Form)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.frame = QFrame(Form)
        self.frame.setObjectName(u"frame")
        self.frame.setStyleSheet(u"QFrame {\n"
"border-radius: 10px;\n"
"}")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.verticalLayout = QVBoxLayout(self.frame)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.labelName = QLabel(self.frame)
        self.labelName.setObjectName(u"labelName")
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelName.sizePolicy().hasHeightForWidth())
        self.labelName.setSizePolicy(sizePolicy)
        self.labelName.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout.addWidget(self.labelName)

        self.editName = QLineEdit(self.frame)
        self.editName.setObjectName(u"editName")

        self.horizontalLayout.addWidget(self.editName)

        self.labelLesson = QLabel(self.frame)
        self.labelLesson.setObjectName(u"labelLesson")
        sizePolicy.setHeightForWidth(self.labelLesson.sizePolicy().hasHeightForWidth())
        self.labelLesson.setSizePolicy(sizePolicy)
        self.labelLesson.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout.addWidget(self.labelLesson)

        self.comboLesson = QComboBox(self.frame)
        self.comboLesson.setObjectName(u"comboLesson")

        self.horizontalLayout.addWidget(self.comboLesson)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.labelDescription = QLabel(self.frame)
        self.labelDescription.setObjectName(u"labelDescription")
        sizePolicy.setHeightForWidth(self.labelDescription.sizePolicy().hasHeightForWidth())
        self.labelDescription.setSizePolicy(sizePolicy)
        self.labelDescription.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout_3.addWidget(self.labelDescription)

        self.editDesc = QLineEdit(self.frame)
        self.editDesc.setObjectName(u"editDesc")

        self.horizontalLayout_3.addWidget(self.editDesc)


        self.verticalLayout.addLayout(self.horizontalLayout_3)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.labelDate = QLabel(self.frame)
        self.labelDate.setObjectName(u"labelDate")
        sizePolicy.setHeightForWidth(self.labelDate.sizePolicy().hasHeightForWidth())
        self.labelDate.setSizePolicy(sizePolicy)
        self.labelDate.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout_4.addWidget(self.labelDate)

        self.editDate = QDateEdit(self.frame)
        self.editDate.setObjectName(u"editDate")

        self.horizontalLayout_4.addWidget(self.editDate)

        self.labelTime = QLabel(self.frame)
        self.labelTime.setObjectName(u"labelTime")
        sizePolicy.setHeightForWidth(self.labelTime.sizePolicy().hasHeightForWidth())
        self.labelTime.setSizePolicy(sizePolicy)
        self.labelTime.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout_4.addWidget(self.labelTime)

        self.editTime = QTimeEdit(self.frame)
        self.editTime.setObjectName(u"editTime")

        self.horizontalLayout_4.addWidget(self.editTime)


        self.verticalLayout.addLayout(self.horizontalLayout_4)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer)

        self.buttonCancel = QPushButton(self.frame)
        self.buttonCancel.setObjectName(u"buttonCancel")

        self.horizontalLayout_2.addWidget(self.buttonCancel)

        self.buttonOK = QPushButton(self.frame)
        self.buttonOK.setObjectName(u"buttonOK")

        self.horizontalLayout_2.addWidget(self.buttonOK)


        self.verticalLayout.addLayout(self.horizontalLayout_2)


        self.verticalLayout_2.addWidget(self.frame)


        self.retranslateUi(Form)

        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Form", None))
        self.labelName.setText(QCoreApplication.translate("Form", u"Name:", None))
        self.labelLesson.setText(QCoreApplication.translate("Form", u"Lesson", None))
        self.labelDescription.setText(QCoreApplication.translate("Form", u"Description:", None))
        self.labelDate.setText(QCoreApplication.translate("Form", u"Date:", None))
        self.labelTime.setText(QCoreApplication.translate("Form", u"Time:", None))
        self.buttonCancel.setText(QCoreApplication.translate("Form", u"Cancel", None))
        self.buttonOK.setText(QCoreApplication.translate("Form", u"Ok", None))
    # retranslateUi

