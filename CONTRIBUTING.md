# Contributing guide

Wow, I'm actually suprised someones reading this. Thanks for your interest!

<u>Issues</u>

If you've discovered a problem with this app, or wish to discuss a feature to be added in the future, please open an issue. This will let me keep track of all the the things that need to be added or fixed.

<u>Code contributions</u>

Before making a merge request, or even writing any code, please open an issue! It will help me keep track of everything thats going on, and if your contribution is a feature, an issue will allow discussion for the best ways to implement it in the UI and how to present the feature to the users.

If the idea has been properly discussed, feel free to fork this repository and implement the code that is required, and then open a merge request back to this repo for it to be added back to main. If you don't open an issue, it's **highly likely** your merge request will be closed.

Thank you for your interest in this project, and happy coding!