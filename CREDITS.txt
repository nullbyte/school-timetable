Author:
Rafael Fryer

Special thanks:
Arabella: My gorgeous girlfriend, moral support and source of ideas
Meenu: moral support
Keely: Tester
The good people of StackOverflow, GeeksForGeeks, and many other websites: Programming tutorials and help

Frameworks and libraries used:
Python: Backend and code
PySide6 (Qt): UI, graphics, and desktop integration
notifypy (and its dependencies): Desktop notifications
psutil: single instance capabilities on Windows

Image assets:
https://www.pinclipart.com/maxpin/oTbohT/
https://picsart.com/i/234283200048211
https://www.flaticon.com/free-icon/hot-dog_405285
Woodcroft College (logo and map)
Commodore (logo)
Ubuntu (logo)
Tela icon theme (Icon set)
Google (Timetable icon)
