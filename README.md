# School timetable

<img src="./assets/screenshots/default.png" alt="screenshot" width="400"/>

### About
This is an app designed to manage student timetables at [Woodcroft College](https://www.woodcroft.sa.edu.au/). It's written in Python and uses the Qt framework for graphics and ui.

### Download
Downloads for both platforms in binary form are available here:

**[Download from OneDrive](https://woodcroftcollege-my.sharepoint.com/:f:/g/personal/fryer_r_woodcroft_sa_edu_au/EvctrbLM66xFiuFxh2GD8EYBKz5movpPkkYvdTr0qkbO8g?e=fzIawK) (NOTE: Next release will likely be directly on Gitea, since it provides binary hosting.)**

This link contains two files, a .exe for Windows and a .app for Linux. Please download the one for your OS.

**NOTE:** Windows users may experience antivirus warnings when attempting to execute the app. This is due to the way Windows trusts programs from different creators, and to circumvent these warnings requires a payment to Microsoft. Rest assured this program contains no malware, and it is safe to dismiss these warnings (For Windows defender this involves pressing "Learn more", then "Run anyway").

### Platforms
The app is developed on and for Linux, with builds also being produced for Windows. MacOS may work in theory, however it has not been tested and builds have not been made.

### Compiling
To build the app, `git clone` this repository then execute the `buildlin.sh` script to build an AppImage bundle. This AppImage only contains the app itself and the libraries needed to run. Python and PySide2 (The python bindings for Qt used in this application) as well as any other dependencies required are not included to save on program space and reduce redundancy.