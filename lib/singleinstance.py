import os, sys
from multiprocessing.connection import Listener, Client
from PySide6.QtCore import QObject, Signal
from threading import Thread
if sys.platform == "win32":
	sys.path.append("../lib-win32")
	import psutil

class Window(QObject):
	show = Signal()
showWindow = Window()

# Yes I copied this from StackOverflow. I'm not that smart

def checkPid(pid):
	if sys.platform == "linux":
		try:
			os.kill(pid, 0)
		except OSError:
			return False
		else:
			return True
	elif sys.platform == "win32":
		return psutil.pid_exists(pid)

def listenForNewInstance():
	with open(os.path.join(confdir, "proc.lock"), 'w') as procfile:
		procfile.write(str(os.getpid()))
	listener = Listener(('localhost', 1104), authkey=b'showtimetable')
	while True:
		conn = listener.accept()
		msg = conn.recv()
		if msg == 'showold':
			print("Showing from new instance")
			showWindow.show.emit()
			msg = ""

def newInstance():
	with open(os.path.join(confdir, "proc.lock"), "w") as procfile:
		procfile.write(str(os.getpid()))
	print("First instance, starting listener")
	Thread(target=lambda: listenForNewInstance()).start()

def start(conf_dir, ):
	global confdir
	confdir = conf_dir
	if os.path.isfile(os.path.join(confdir, "proc.lock")):
		with open(os.path.join(confdir, "proc.lock"), "r") as procfile:
			oldpid = procfile.read()
		if oldpid != "" and checkPid(int(oldpid)):
			print("Timetable already running")
			with Client(('localhost', 1104), authkey=b'showtimetable') as conn:
				conn.send('showold')
				conn.close()
			os._exit(0)
		else:
			newInstance()
	else:
		newInstance()
