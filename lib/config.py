import sys, os
from configparser import ConfigParser
from PySide6.QtCore import QStandardPaths

confdir = os.path.join(QStandardPaths.standardLocations(QStandardPaths.ConfigLocation)[0], 'timetable')
subjectsCfg = ConfigParser(strict=False, interpolation=None)
settingsCfg = ConfigParser(strict=False, interpolation=None)
assignmentsCfg = ConfigParser(strict=False, interpolation=None)

def load(customfile=""):
	global confdir, tablefile, configfile, assignmentsfile

	if sys.platform == 'win32' and not os.path.isdir(QStandardPaths.standardLocations(QStandardPaths.ConfigLocation)[0]):
		os.mkdir(QStandardPaths.standardLocations(QStandardPaths.ConfigLocation)[0])
	if not os.path.isdir(confdir):
		os.mkdir(confdir)

	if customfile == "":
		tablefile = os.path.join(confdir, 'timetable.tbl')
	else:
		tablefile = customfile
	
	configfile = os.path.join(confdir, 'timetable.cfg')
	assignmentsfile = os.path.join(confdir, 'assignments.tbl')

	if not os.path.isdir(confdir):
		os.mkdir(confdir)

	if not os.path.isfile(tablefile):
		open(tablefile, 'x').close()
		print("Created empty configuration file in " + tablefile)
	if not os.path.isfile(configfile):
		open(configfile, 'x').close()
		print("Created empty configuration file in " + configfile)
	if not os.path.isfile(assignmentsfile):
		open(assignmentsfile, 'x').close()
		print("Created empty configuration file in " + assignmentsfile)
	subjectsCfg.read(tablefile)
	settingsCfg.read(configfile)
	assignmentsCfg.read(assignmentsfile)
	print("Loaded configuration")

def writeSubjects():
	with open(tablefile, 'w') as tmpcfg:
		subjectsCfg.write(tmpcfg)
	print("Timetable saved to drive")
def writeSettings():
	with open(configfile, 'w') as tmpcfg:
		settingsCfg.write(tmpcfg)
	print("Settings saved to drive")
def writeAssignments():
	with open(assignmentsfile, 'w') as tmpcfg:
		assignmentsCfg.write(tmpcfg)
	print("Assignments saved to drive")