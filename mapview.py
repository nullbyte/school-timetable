import sys, os
from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *

os.chdir(os.path.dirname(os.path.realpath(__file__)))
sys.path.append("lib")
sys.path.append("assets/ui")
if sys.platform == "win32":
	sys.path.append("lib-win32")

import config, lessonpoints

from map_ui import Ui_Dialog as map_ui_Dialog


Window = QDialog()
window = map_ui_Dialog()
window.setupUi(Window)
Window.setAttribute(Qt.WA_TranslucentBackground)
Window.setWindowFlags(Qt.FramelessWindowHint | Qt.Tool)
Window.setWindowTitle("School Map")

app: QApplication
selectedTime = 0

mapExists = False

def showMap(mapmode=0):
	global mapImage, mapExists, mapWidth, mapHeight

	if not mapExists:
		mapExists = True
		# Make and render map
		rawMapImage = QPixmap(":/backgrounds/map.png")
		mapWidth = rawMapImage.width()
		mapHeight = rawMapImage.height()
		mapImage = rawMapImage.scaled(mapWidth / 2 * app.devicePixelRatio(), mapHeight / 2 * app.devicePixelRatio(), Qt.KeepAspectRatio)
		del rawMapImage
		mapImage.setDevicePixelRatio(app.devicePixelRatio())
	renderMap(mode=mapmode, w=mapWidth, h=mapHeight)
	Window.show()
	# From subject view
	if mapmode == 1 and config.subjectsCfg.has_option(selectedTime.day + str(selectedTime.lesson), "Room") and config.subjectsCfg[selectedTime.day + str(selectedTime.lesson)]["Room"].lower() in lessonpoints.x and config.subjectsCfg[selectedTime.day + str(selectedTime.lesson)]["Room"].lower() in lessonpoints.y:
		#print((lessonpoints.x[config.subjectsCfg[selectedTime.day + str(selectedTime.lesson)]["Room"].lower()] / 2) - (Window.width() / 2), (lessonpoints.y[config.subjectsCfg[selectedTime.day + str(selectedTime.lesson)]["Room"].lower()] / 2) - (Window.height() / 2))
		window.map.horizontalScrollBar().setValue((lessonpoints.x[config.subjectsCfg[selectedTime.day + str(selectedTime.lesson)]["Room"].lower()] / 2) - (Window.width() / 2))
		window.map.verticalScrollBar().setValue((lessonpoints.y[config.subjectsCfg[selectedTime.day + str(selectedTime.lesson)]["Room"].lower()] / 2) - (Window.height() / 2))
		window.labelTitle.setText("Location of the " + config.subjectsCfg[selectedTime.day + str(selectedTime.lesson)]["SubjectName"] + " room")
	# From "Find" button
	elif mapmode == 2 and window.editLocation.text() and window.editLocation.text().lower() in lessonpoints.x and window.editLocation.text().lower() in lessonpoints.y:
		window.map.horizontalScrollBar().setValue((lessonpoints.x[window.editLocation.text().lower()] / 2) - (Window.width() / 2))
		window.map.verticalScrollBar().setValue((lessonpoints.y[window.editLocation.text().lower()] / 2) - (Window.height() / 2))
		window.labelTitle.setText("Location of room " + window.editLocation.text())
	# From main view
	elif mapmode == 0:
		window.map.horizontalScrollBar().setValue(window.map.horizontalScrollBar().maximum() / 2)
		window.map.verticalScrollBar().setValue(window.map.verticalScrollBar().maximum() / 2)
		window.labelTitle.setText("School map")
	# Room exists in one but not all fields
	elif (mapmode == 2 and (map.editLocation.text() in lessonpoints.x or map.editLocation.text() in lessonpoints.y)) or (mapmode == 1 and (config.subjectsCfg.has_option(selectedTime.day + str(selectedTime.lesson), "Room") and config.subjectsCfg[selectedTime.day + str(selectedTime.lesson)]["Room"].lower() in lessonpoints.x or config.subjectsCfg[selectedTime.day + str(selectedTime.lesson)]["Room"].lower() in lessonpoints.y)):
		QMessageBox.warning(timetableWindow, "Malformed room number", "The room entered exists, but is not configured correctly. Please report this as a bug on https://gitlab.com/squidink7/school-timetable/.", QMessageBox.Ok)
		return
	# Room doesn't exist
	else:
		QMessageBox.information(Window, "Room not found", "The room entered does not match any known classrooms, please check the number and try again.", QMessageBox.Ok)
		return

window.buttonFind.clicked.connect(lambda: showMap(2))

# Map graphics scene
mapScene = QGraphicsScene()
window.map.setScene(mapScene)

def renderMap(event=None, mode=0, w=0, h=0):
	mapScene.clear()
	mapScene.addPixmap(mapImage)
	if mode == 1 and config.subjectsCfg[selectedTime.day + str(selectedTime.lesson)]["Room"].lower() in lessonpoints.x and config.subjectsCfg[selectedTime.day + str(selectedTime.lesson)]["Room"].lower() in lessonpoints.y:
		# Draw dot from room
		mapScene.addEllipse((lessonpoints.x[config.subjectsCfg[selectedTime.day + str(selectedTime.lesson)]["Room"].lower()] / 2) - 10, (lessonpoints.y[config.subjectsCfg[selectedTime.day + str(selectedTime.lesson)]["Room"].lower()] / 2) - 10, 20, 20, QPen(Qt.SolidLine), QBrush(Qt.red, Qt.SolidPattern))
	elif mode == 2 and window.editLocation.text().lower() in lessonpoints.x and window.editLocation.text().lower() in lessonpoints.y:
		# Draw dot from manual entry
		mapScene.addEllipse((lessonpoints.x[window.editLocation.text().lower()] / 2) - 10, (lessonpoints.y[window.editLocation.text().lower()] / 2) - 10, 20, 20, QPen(Qt.SolidLine), QBrush(Qt.red, Qt.SolidPattern))

def resetMap():
	if config.subjectsCfg.has_option(selectedTime.day + str(selectedTime.lesson), "Room") and config.subjectsCfg[selectedTime.day + str(selectedTime.lesson)]["Room"].lower() in lessonpoints.x and config.subjectsCfg[selectedTime.day + str(selectedTime.lesson)]["Room"].lower() in lessonpoints.y:
		window.map.horizontalScrollBar().setValue((((lessonpoints.x[config.subjectsCfg[selectedTime.day + str(selectedTime.lesson)]["Room"].lower()] / 2) - (Window.width() / 2)) / w * 2173) + 30)
		window.map.verticalScrollBar().setValue((((lessonpoints.y[config.subjectsCfg[selectedTime.day + str(selectedTime.lesson)]["Room"].lower()] / 2) - (Window.height() / 2)) / h * 1555) + 30)
window.buttonReset.clicked.connect(resetMap)

def hideMap():
	Window.close()
window.buttonClose.clicked.connect(hideMap)
